﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeMonkey.Utils;
using TMPro;

public class UI_Inventory : MonoBehaviour {

    private Inventory inventory;
    public Transform itemSlotContainer;
    public Transform itemSlotTemplate;
    private Player player;

    public int currentammo { get; set; }
    public FireGun gun;
    // para llamar a la funcion recuperar Health
    public CharacterHealth characterHealth;
    // para el cambio de arma pistola y cuchillo 
    public PlayerTPSController cambio;
    public PlayerFPSController cambio_de_arma;
    // Desactivar armas 
    public GameObject pistola;
    public GameObject cuchillo;
    private void Awake() {
        itemSlotContainer = transform.Find("itemSlotContainer");
        itemSlotTemplate = itemSlotContainer.Find("itemSlotTemplate");
        cuchillo.SetActive(false);
    }
    private void Update()
    {
        currentammo=inventory.Getammo();
        gun.currentAmmo = currentammo;
        //print(currentammo);
    }

    public void SetPlayer(Player player) {
        this.player = player;
    }

    public void SetInventory(Inventory inventory) {
        this.inventory = inventory;

        inventory.OnItemListChanged += Inventory_OnItemListChanged;

        RefreshInventoryItems();
    }

    private void Inventory_OnItemListChanged(object sender, System.EventArgs e) {
        RefreshInventoryItems();
    }
    public void Gunremovebullets()
    {
        inventory.RemoveGunbullets();
    }
    private void RefreshInventoryItems() {
        foreach (Transform child in itemSlotContainer) {
            if (child == itemSlotTemplate) continue;
            Destroy(child.gameObject);
        }

        int x = 0;
        int y = 0;
        float itemSlotCellSize = 75f;
        foreach (Item item in inventory.GetItemList()) {
            RectTransform itemSlotRectTransform = Instantiate(itemSlotTemplate, itemSlotContainer).GetComponent<RectTransform>();
            itemSlotRectTransform.gameObject.SetActive(true);
            
            itemSlotRectTransform.GetComponent<Button_UI>().ClickFunc = () => {
                // Use item
                inventory.UseItem(item);
                if (item.name== "ManaPotion")
                {
                    inventory.AddMunicion(item);                 
                }
                if (item.name == "Health Potion")
                {
                    characterHealth.Restore();
                }
                if (item.name=="Sword")//cuchillo
                {
                    pistola.SetActive(false);
                    cuchillo.SetActive(true);
                    cambio_de_arma.cuchillo_activada();
                    cambio.input_r = false;
                }
                if (item.name == "Coin")//arma
                {
                    pistola.SetActive(true);
                    cuchillo.SetActive(false);
                    cambio_de_arma.pistola_activada();
                    cambio.input_r = true;
                }
            };
            itemSlotRectTransform.GetComponent<Button_UI>().MouseRightClickFunc = () => {
                // Drop item
                Item duplicateItem = new Item { itemType = item.itemType, amount = item.amount };
                inventory.RemoveItem(item);
                ItemWorld.DropItem(player.GetPosition(), duplicateItem);
            };

            itemSlotRectTransform.anchoredPosition = new Vector2(x * itemSlotCellSize, -y * itemSlotCellSize);
            Image image = itemSlotRectTransform.Find("image").GetComponent<Image>();
            image.sprite = item.GetSprite();

            TextMeshProUGUI uiText = itemSlotRectTransform.Find("amountText").GetComponent<TextMeshProUGUI>();
            if (item.amount >=1) {// para que se vea el zero //antes habia un > 1 
                uiText.SetText(item.amount.ToString());
            } else {
                uiText.SetText("0");
            }

            x++;
            if (x >= 4) {
                x = 0;
                y++;
            }
        }
    }


}
