﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHurt : MonoBehaviour {
    public int damage;
    // Use this for initialization
    //animacion de ataque del child 
    public int hit=0;
    public  int guardar = 0;
    private void OnTriggerStay(Collider other)
    {
        
        if (other.gameObject.CompareTag("Player"))
        {
            other.GetComponent<CharacterHealth>().hurt(damage);
            guardar = 1;
        }
        hit = guardar;
    }
    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.CompareTag("Player"))
        {
            other.GetComponent<CharacterHealth>().hurt(damage);
            guardar = 0;
        }
        hit = guardar;
    }
}
