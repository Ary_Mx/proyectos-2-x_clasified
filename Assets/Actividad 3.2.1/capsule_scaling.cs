﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class capsule_scaling : MonoBehaviour {
    [SerializeField]
    private Vector3 axes;
    public float scaleUnits;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        axes = capsule_movement_C.clampVector3(axes);
        transform.localScale = transform.localScale + axes * (scaleUnits * Time.deltaTime);
    }
}
