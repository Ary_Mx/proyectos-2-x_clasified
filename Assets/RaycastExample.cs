﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaycastExample : MonoBehaviour {

   [Range(0.01f,250)]
    public float distance = 100;
	
	// Update is called once per frame
	void Update () {
        Vector3 fwd = transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(transform.position, fwd, distance))
            print("Ther is something in front of the object!");
	}
}
