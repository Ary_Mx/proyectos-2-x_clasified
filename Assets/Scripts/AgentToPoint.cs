﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class AgentToPoint : MonoBehaviour {
    public GameObject target;
    private NavMeshAgent navAgent;
    public bool followTarget { get; set; }
    public bool followFromStart = false;
    public Vector3 lastpositionplayer;
    // Use this for initialization
     EnemyController zombie;
    // si el  zombie se detiene la animacion tambien 
    public int parar = 0;
    public int guardar = 0;

    public int damage;
    // Use this for initialization
    //animacion de ataque del child 
    public int hit = 0;
    public int guardar_a = 0;
    // tiempo para que ataque
    bool after = false;
    void Start () {
        zombie = GetComponent<EnemyController>();
        navAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (target&& followFromStart)
        {
            navAgent.SetDestination(target.transform.position);

        }
       
    }
	
	// controlamos el movimiento no la animacion 
	void Update () {
    
      // print(followTarget);
        if (zombie.death == 1)
        {
            followTarget = false;
        }

        //  navAgent.SetDestination(transform.position);



        if (followTarget)
            {
            parar = 1;
                navAgent.SetDestination(target.transform.position);
                lastpositionplayer = target.transform.position;
           // print("hola1");
        }
        
        if (!followTarget)
        {
           // print("hola2");
            if (lastpositionplayer.x==transform.position.x&& lastpositionplayer.z == transform.position.z)
            {
           //     print("hola3");
                parar = 0;
                navAgent.SetDestination(transform.position);  
            }
  

        }
	}
    
  
}
