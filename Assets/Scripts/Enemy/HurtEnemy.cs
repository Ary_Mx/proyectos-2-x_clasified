﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemy : MonoBehaviour
{
    public int damage;
    public GameObject cutImpactPrefab;
    // Use this for initialization
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
           // Debug.Log("Ouch");
            other.GetComponent<CharacterHealth>().hurt(damage);
            GameObject hit=Instantiate(cutImpactPrefab,transform.position,transform.rotation);
        }
    }
}
