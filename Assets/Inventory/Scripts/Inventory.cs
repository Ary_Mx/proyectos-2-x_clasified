﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory {

    public event EventHandler OnItemListChanged;

    private List<Item> itemList;
    private Action<Item> useItemAction;

    public Inventory(Action<Item> useItemAction) {
        this.useItemAction = useItemAction;
        itemList = new List<Item>();

        // AddItem(new Item { itemType = Item.ItemType.Sword, amount = 1, name="Sword" });
        //   AddItem(new Item { itemType = Item.ItemType.HealthPotion, amount = 1,name="HealthPotion" });
        //  AddItem(new Item { itemType = Item.ItemType.ManaPotion, amount = 1 ,name="ManaPotion"});
        AddItem(new Item { itemType = Item.ItemType.Coin, amount = 0, name = "Coin" });
    }
    // Aqui obtenemos el ammo 

    public int Getammo()
    {
        foreach (Item inventoryItem in itemList)
        {
            if (inventoryItem.name == "Coin")
            {
                return inventoryItem.amount;
            }
        }
        return 1993;
    }
    public void AddItem(Item item) {
        if (item.IsStackable()) {
            bool itemAlreadyInInventory = false;
            foreach (Item inventoryItem in itemList) {
                if (inventoryItem.itemType == item.itemType) {
                    inventoryItem.amount += item.amount;//aquiiiiiiiiii                  
                    itemAlreadyInInventory = true;
                }
            }
            if (!itemAlreadyInInventory) {
                itemList.Add(item);
            }
        } else {
            itemList.Add(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }
    // Add Municiones
    public  void AddMunicion(Item item)
    {
            foreach (Item inventoryItem in itemList)
            {
                if (inventoryItem.name == "Coin")
                {
                      inventoryItem.amount += 1;//aquiiiiiiiiii
                }
            }     
        OnItemListChanged?.Invoke(this, EventArgs.Empty);// Que es esto?
    }
    // Prueba  Municion
    public void prueba()
    {
        
        Debug.Log("Click pistola");
    }
    public void RemoveItem(Item item) {
        if (item.IsStackable()) {
            Item itemInInventory = null;
            foreach (Item inventoryItem in itemList) {
                if (inventoryItem.itemType == item.itemType) {
                    inventoryItem.amount -= item.amount;
                    itemInInventory = inventoryItem;
                }
            }
            if (itemInInventory != null && itemInInventory.amount <= 0) {
                itemList.Remove(itemInInventory);
            }
        } else {
            itemList.Remove(item);
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);
    }
    public void RemoveGunbullets()
    {
       
        foreach (Item inventoryItem in itemList)
        {
            if (inventoryItem.name == "Coin")
            {
                if (inventoryItem.amount != 0)
                {
                    inventoryItem.amount -= 1;//aquiiiiiiiiii
                }            
                else
                {
                    inventoryItem.amount = 0;
                }
            }
        }
        OnItemListChanged?.Invoke(this, EventArgs.Empty);// Que es esto?
    }
    public void UseItem(Item item) {
        useItemAction(item);
    }

    public List<Item> GetItemList() {
        return itemList;
    }

}
