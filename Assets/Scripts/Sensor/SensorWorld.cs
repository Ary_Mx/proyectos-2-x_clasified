﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorWorld : MonoBehaviour
{
    public GameObject Camara_Cambiante;
    public GameObject Camara_Fija_1;
    public GameObject Camara_Fija_2;
    public GameObject Camara_Fija_3;
    public GameObject Camara_Fija_4;
    public GameObject Camara_Fija_5;
    public GameObject Camara_Fija_6;
    public GameObject Camara_Fija_7;
    public GameObject Camara_Fija_8;
    public GameObject Camara_Fija_9;
    public GameObject Camara_Fija_10;
    public GameObject Camara_Fija_11;
    public GameObject Camara_Fija_12;
    public GameObject Camara_Fija_13;
    public GameObject Camara_Fija_14;
    public GameObject Camara_Fija_15;
    static int entrando=0;
    public void SensorEntrada(string nombre_del_sensor)
    {
        if (nombre_del_sensor=="Sensor_Camaras_Fijas_2")
        {
                Camara_Cambiante.transform.position = Camara_Fija_2.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_2.GetComponent<Transform>().rotation;
        }
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_1")
        {
            Camara_Cambiante.transform.position = Camara_Fija_1.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation= Camara_Fija_1.GetComponent<Transform>().rotation;
        }
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_3")
        {
            Camara_Cambiante.transform.position = Camara_Fija_1.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_1.GetComponent<Transform>().rotation;
        }
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_4")
        {
            Camara_Cambiante.transform.position = Camara_Fija_3.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_3.GetComponent<Transform>().rotation;
        }
        // para la camara 3-4 
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_5")
        {
            Camara_Cambiante.transform.position = Camara_Fija_3.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_3.GetComponent<Transform>().rotation;
        }
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_6")
        {
            Camara_Cambiante.transform.position = Camara_Fija_4.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_4.GetComponent<Transform>().rotation;
        }
        //para camara 4-5
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_7")
        {
            Camara_Cambiante.transform.position = Camara_Fija_4.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_4.GetComponent<Transform>().rotation;
        }
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_8")
        {
            Camara_Cambiante.transform.position = Camara_Fija_5.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_5.GetComponent<Transform>().rotation;
        }
        //respaldo para camara 5
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_9")
        {
            Camara_Cambiante.transform.position = Camara_Fija_5.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_5.GetComponent<Transform>().rotation;
        }
        //camara 6
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_10")
        {
            Camara_Cambiante.transform.position = Camara_Fija_6.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_6.GetComponent<Transform>().rotation;
        }
        //camara 7
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_11")
        {
            Camara_Cambiante.transform.position = Camara_Fija_7.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_7.GetComponent<Transform>().rotation;
        }
        //camara 8
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_12")
        {
            Camara_Cambiante.transform.position = Camara_Fija_8.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_8.GetComponent<Transform>().rotation;
        }
        //camara 9
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_13")
        {
            Camara_Cambiante.transform.position = Camara_Fija_9.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_9.GetComponent<Transform>().rotation;
        }
        //camara 9 respaldos
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_15")
        {
            Camara_Cambiante.transform.position = Camara_Fija_9.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_9.GetComponent<Transform>().rotation;
        }
        //camara 10
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_14")
        {
            Camara_Cambiante.transform.position = Camara_Fija_10.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_10.GetComponent<Transform>().rotation;
        }
        //camara 11
        if (nombre_del_sensor == "Sensor_Camaras_Fijas_16")
        {
            Camara_Cambiante.transform.position = Camara_Fija_11.GetComponent<Transform>().position;
            Camara_Cambiante.transform.rotation = Camara_Fija_11.GetComponent<Transform>().rotation;
        }
    }

}
