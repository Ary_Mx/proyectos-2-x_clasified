﻿
using UnityEngine;

public class MouseLook : MonoBehaviour {

    public GameObject camerasParent;         // Parent object of all cameras that should rotate with mouse
    public float hRotationSpeed = 100f;      // Player rotates along y axis
    public float vRotationSpeed = 80f;       // Cam rotates along x axis
    public float maxVerticalAngle;           // maximun rotation along x axis
    public float minVerticalAngle;           // minimum rotation along x axis
    public float maxHorizontalAngle;           // maximun rotation along x axis
    public float minHorizontalAngle;           // minimum rotation along x axis
    public float smoothTime = 0.05f;

    float vCamRotationAngles;                // variable to apply Vertical Rotation
    float hCamRotationAngles;                // variable to apply Vertical Rotation
    float hPlayerRotation;                   // variable to apply Horizontal Rotation
    float currentHVelocity;                  // smooth horizonal velocity
    float currentVVelocity;                  // smooth vertica velocity
    float xtargetCamEulers;                   // variable to accumulate the eurler angles along x axis
    float ytargetCamEulers;                   // variable to accumulate the eurler angles along y axis
    Vector3 targetCamRotation;               /* aux variable to store the targetRotation of the 
                                             camerasParent avoiding to instatiate a new Vector 3 every Frame */
    public float verticalAxis { get;  set; }
    public float horizontalAxis { get; set; }

    // para la animacion 
    public GameObject Cha35;
    private float Speed;
    private Animator animation_other_object;
    // parametro booleano que controla transicion de caminar a disparar
    //bool aim_now;
    void Start () {
        //Instanciamos el objeto de la clase animation 
        animation_other_object = Cha35.GetComponent<Animator>();
        // Hide and lock mouse cursor
        //Cursor.visible = false;// aqui esta para ocultar el cursor 
       // Cursor.lockState = CursorLockMode.Locked;
    }


	void Update () {
        // Get Mouse input horizontal axis
        xtargetCamEulers += horizontalAxis * hRotationSpeed * Time.deltaTime;

        // Get Mouse input vertical axis
        ytargetCamEulers += verticalAxis * vRotationSpeed * Time.deltaTime;

        // Player Rotation
        float targetPlayerRotation = horizontalAxis * hRotationSpeed * Time.deltaTime;
        hPlayerRotation = Mathf.SmoothDamp(hPlayerRotation, targetPlayerRotation, ref currentHVelocity, smoothTime);
      transform.Rotate(0f, hPlayerRotation, 0f);

        //Cam Rotation horizontal axis 
        xtargetCamEulers = Mathf.Clamp(xtargetCamEulers, minHorizontalAngle, maxHorizontalAngle);
        hCamRotationAngles = Mathf.SmoothDamp(hCamRotationAngles, xtargetCamEulers, ref currentHVelocity, smoothTime);

        //Cam Rotation vertical axis 
        ytargetCamEulers = Mathf.Clamp(ytargetCamEulers, minVerticalAngle, maxVerticalAngle);
        vCamRotationAngles = Mathf.SmoothDamp(vCamRotationAngles,ytargetCamEulers, ref currentVVelocity, smoothTime);

       // targetCamRotation.Set(-vCamRotationAngles, hCamRotationAngles, 0f);
        targetCamRotation.Set(-vCamRotationAngles,0f, 0f);

        //Calculate the Input Magnitude
       // Speed = new Vector2(hRotationSpeed, vRotationSpeed).normalized.sqrMagnitude;
       // print("velocidad");
        Speed = vCamRotationAngles /maxVerticalAngle;
       // print(Speed);


        animation_other_object.SetFloat("Blend",Speed);
        //Cha35.transform.Rotate(0f,hPlayerRotation, 0f);
       camerasParent.transform.localEulerAngles = targetCamRotation;// al que tenemos que girar es al soldado

    }
}


