﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerTPSController : MonoBehaviour
{
    public Camera cam;
    public UnityEvent onInteractionInput;
    private InputData input;
    private CharacterAnimBasedMovement characterMovement;
    
    public bool onInteractionZone { get; set; }
    public bool Pausa { get; set; }
    // Ary
    public PlayerInteraction interaction;
    // desde el UI para desabilitar aim o el input r del arma al hacer el cambio a cuchillo 
    public bool input_r { get; set; }
    public GameObject cortar;
    private float timer = 0.0f;
    public float wait_time = 0.4f;
    void Start()
    {
        characterMovement = GetComponent<CharacterAnimBasedMovement>();
        input_r = true;
    }

    // Update is called once per frame
    void Update()
    {
        print(Pausa);
        if (Pausa)
        {
            characterMovement.moveCharacter(0.0f, 0.0f,cam, false, false, false, false);
            if (onInteractionZone)
            {
                onInteractionInput.Invoke();
            }
        }
        if (!Pausa)
        {
            input.getInput();
       
            
        
         
        if (!input.inv)
        {

           
            // interaction_wall = interaction.InteractRaycastAlternative(); }
           
                // print(input.aim);
                if (input_r)
                {

                    characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash,input.aim,false);
                }
                if (!input_r)
                {
                    characterMovement.moveCharacter(input.hMovement, input.vMovement, cam, input.jump, input.dash,false,false);
                    if (Input.GetMouseButtonDown(0))
                    {
                        cortar.SetActive(true);
                        characterMovement.moveCharacter(0.0f, 0.0f, cam, false, false, false,true);
                    }
                    timer += Time.deltaTime;
                    if (timer > wait_time)
                    {
                        cortar.SetActive(false);
                        timer = 0.0f;
                    }
                }
              
            
        }
        else
        {
            characterMovement.moveCharacter(0.0f, 0.0f, cam,false,false,false,false);
        }
        }
    }
}
