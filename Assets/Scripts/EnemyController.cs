﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    private CharacterHealth characterHealth;
    //  private GameObject enemy;
    public int points;
    // Use this for initialization
    // animacion de muerte
    public int death=0;
    public int guardar=0;
    public Collider m1_Collider;
    public Collider m2_Collider;
    public AgentToPoint foolow;
    void Start () {
        characterHealth = this.gameObject.GetComponent<CharacterHealth>();
        m1_Collider = this.gameObject.GetComponent<Collider>();
        foolow = this.gameObject.GetComponent<AgentToPoint>();
    }
	
	// Update is called once per frame
	void Update () {
        //this.gameObject.SetActive(true);
        if (characterHealth.isDead)
        {
            GameController.score += points;
            // Destroy(gameObject, 0.2f);
            characterHealth.revive();
            characterHealth.Health_After_Revive();
            guardar= 1;
            m1_Collider.enabled = !m1_Collider.enabled;
            m2_Collider.enabled = !m2_Collider.enabled;
            foolow.followTarget = false;
            //this.gameObject.SetActive(false);
            //enabled = false;
        }
        death=guardar;
	}

    public void respawner(Vector3 pos)
    {
        this.gameObject.GetComponentInChildren<MeshRenderer>().enabled = false;//adios blood clones
    }
}
