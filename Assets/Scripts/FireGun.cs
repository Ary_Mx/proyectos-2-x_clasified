﻿using System.Collections;
using UnityEngine;

[RequireComponent (typeof(AudioSource), typeof(BulletHitController))]
public class FireGun : MonoBehaviour {
  
    [Header("Gun Specs")]
    public FireGunData gunData = new FireGunData(10f, 0.1f, 600f, 350f, 30, 1f);

    private float firingTimer;
    private bool isReloading = false;

    public Camera fpsCamera;
    
    private Recoiler gunRecoiler;
    private Recoiler camRecoiler;
    private FireGunFXController fxController;
    private BulletHitController bulletHitController;
    public CharacterHealth characterHealth;

    private AudioSource audioSource;
   
    public int currentAmmo { get;  set; }
    public bool triggerPulled { get; set; }
    public bool isShooting { get; private set; }
    public float recoilerMultiplier { get; set; }
    public bool reloadButton { get; set; }

    // para la reduccion de las balas.
    public UI_Inventory inventario;
    private void Awake() {
        gunRecoiler = GetComponentInParent<Recoiler>();
        camRecoiler = fpsCamera.GetComponentInParent<Recoiler>();
        audioSource = GetComponent<AudioSource>();
        fxController = GetComponentInChildren<FireGunFXController>();
        bulletHitController = GetComponent<BulletHitController>();

        currentAmmo = gunData.magazineCapacity;
    }

    void Update() {
        if (isReloading)
            return;

        if (currentAmmo <= 0 || reloadButton) {
         //   StartCoroutine(reload());// no es necesario recargar 
            return;
        }

        if(characterHealth.isDead==false)
        {
            shoot();//playerfpscontroller aqui
        }
            

            
        
        
    }

    private void shoot() {

        if (Time.time >= firingTimer && triggerPulled) {

            isShooting = true;
            firingTimer = Time.time + 60f / gunData.fireRate;

            inventario.Gunremovebullets();
            
            currentAmmo--;

            //FX
            audioSource.PlayOneShot(gunData.shootFX);
            fxController.Play();

            Vector2 screenCenterPoint = new Vector2(Screen.width / 2, Screen.height / 2);
            gunRecoiler.recoil += gunData.recoil * recoilerMultiplier;
            camRecoiler.recoil += gunData.recoil * recoilerMultiplier;

            // The method ScreenPointToRay needs to be called from a camera
            bulletHitController.handleHit(fpsCamera.ScreenPointToRay(screenCenterPoint), gunData.range, gunData.power);
        }
        else {
            isShooting = false;
        }
    }

    private IEnumerator reload() {
        isReloading = true;
     //   Debug.Log("Reloading");
        audioSource.PlayOneShot(gunData.reloadingSound);

        yield return new WaitForSeconds(gunData.reloadTime);
        
        currentAmmo = gunData.magazineCapacity;
        isReloading = false;
    }

    [System.Serializable]
    public struct FireGunData {
        public float power;
        public float recoil;
        public float fireRate;     // in BPM (Bullets per Minute)
        public float range;        // Absolute range, in meters
        public int magazineCapacity;
        public float reloadTime;

        public AudioClip shootFX;
        public AudioClip reloadingSound;

        public FireGunData(float power, float recoil, float fireRate, float range, int magazineCapacity, float reloadTime) {
            this.power = power;
            this.recoil = recoil;
            this.fireRate = fireRate;     // in BPM (Bullets per Minute)
            this.range = range;        // Absolute range, in meters
            this.magazineCapacity = magazineCapacity;
            this.reloadTime = reloadTime;
            shootFX = null;
            reloadingSound = null;
        }
    }
}


