﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieBasedMovement : MonoBehaviour
{


    [Header("Animator Parameters")]
    public string motionParam = "motion";

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;


    private Animator animator;

    // tiempo para dejarte de buscar
    float time=0.0f;
    float waittime = 1.0f;

    // parar caminar o seguir atacando
    bool attack;
    void Start()
    {
        animator = this.gameObject.GetComponent<Animator>();
      
    }

   void Update()
    {
        EnemyController zombie = gameObject.GetComponentInParent(typeof(EnemyController)) as EnemyController;
        AgentToPoint perseguir = gameObject.GetComponentInParent(typeof(AgentToPoint)) as AgentToPoint;
       PlayerHurt atack = gameObject.GetComponentInParent(typeof(PlayerHurt)) as PlayerHurt;
      //  print(perseguir.parar);
        if (atack.hit==1)
        {
            attack = true;
            animator.SetBool("attack",attack);
        }
        if (atack.hit == 0)
        {
            attack = false;
            animator.SetBool("attack",attack);
        }
        if (zombie.death == 1)
        {
            animator.SetBool("death",true);
        }
        if (zombie.death == 0&&perseguir.parar==0)
        {
            //ultima_vez_que_te_vio();    
            animator.SetBool(motionParam,false);
        }
        if (zombie.death == 0 && perseguir.parar ==1)
        {
            // ultima_vez_que_te_vio();    
            animator.SetBool(motionParam,true);
        }



    }

    public void ultima_vez_que_te_vio()
    {
        AgentToPoint speed = gameObject.GetComponentInParent(typeof(AgentToPoint)) as AgentToPoint;
       // print(speed.transform.position);
       // print(speed.lastpositionplayer);
        if (speed.lastpositionplayer.x == speed.transform.position.x && speed.lastpositionplayer.z == speed.transform.position.z)//solo te sigue en x y en z no en y 
        {
           
            moveCharacter(false);
        }
       
    }
    public void moveCharacter(bool move)
    {
       // print(move);
        animator.SetBool(motionParam,move);
    }
    private void OnTriggerEnter(Collider collider)
    {
     
        AgentToPoint speed = gameObject.GetComponentInParent(typeof(AgentToPoint)) as AgentToPoint;
        //print(speed.hit);
        if (collider.gameObject.tag == "Player"&&speed.hit==0)
        {
           // Debug.Log("Buscar Player");
           
            speed.followTarget = true;
           moveCharacter(true);
        }

    }
    private void OnTriggerExit(Collider collider)
    {
        AgentToPoint speed = gameObject.GetComponentInParent(typeof(AgentToPoint)) as AgentToPoint;
      //  print(speed.hit);
        if (collider.gameObject.tag == "Player")// que el objeto y el collider del cambio de camara no este juntos 
        {
            //Debug.Log("Ignorar Player");
      
            speed.followTarget = false;
          
          
        }
    }
}
